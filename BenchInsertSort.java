/**
*
* BenchInsertSort class
*
* print number of steps and time for insertion sort
*
* done using integer arrays of different length
*
* print output in - csv - format arrLength,steps,time
*
* ----- compile -----
*
* javac BenchInsertSort.java
*
* ----- run -----
*
* java BenchInsertSort
*
* ----- example output -----
*
* arrLength,steps,time
* 1000,247289,13
* 10000,25197537,35
* 100000,2493416921,2129
*
*/
class BenchInsertSort{
    public static void main(String[] args){
        int sizea=1000;
        int sizeb=10000;
        int sizec=100000;
        Bench ba=new Bench();
        Bench bb=new Bench();
        Bench bc=new Bench();
        int[] arra=new int[sizea];
        int[] arrb=new int[sizeb];
        int[] arrc=new int[sizec];
        double max=1000.00; // maximum value of an item in array
        int i=0;
        InsertSort sort=new InsertSort();
        long timea=0;
        long timeb=0;
        long timec=0;

        System.out.println("arrLength,steps,time");
        for(i=0;i<arra.length;i++){
            arra[i]=(int)(Math.random()*max);
        }
        for(i=0;i<arrb.length;i++){
            arrb[i]=(int)(Math.random()*max);
        }
        for(i=0;i<arrc.length;i++){
            arrc[i]=(int)(Math.random()*max);
        }
        timea=System.currentTimeMillis();
        sort.sort(arra,ba);
        timea=System.currentTimeMillis()-timea;
        timeb=System.currentTimeMillis();
        sort.sort(arrb,bb);
        timeb=System.currentTimeMillis()-timeb;
        timec=System.currentTimeMillis();
        sort.sort(arrc,bc);
        timec=System.currentTimeMillis()-timec;
        System.out.println(sizea+","+ba.count+","+timea);
        System.out.println(sizeb+","+bb.count+","+timeb);
        System.out.println(sizec+","+bc.count+","+timec);
    }
}
