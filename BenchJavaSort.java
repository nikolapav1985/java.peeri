import java.util.Arrays;

/**
*
* BenchJavaSort class
*
* print number of steps and time for default java sort
*
* done using integer arrays of different length
*
* print output in - csv - format arrLength,steps,time
*
* ----- compile -----
*
* javac BenchJavaSort.java
*
* ----- run -----
*
* java BenchJavaSort
*
* ----- example output -----
*
* arrLength,steps,time
* 1000,-1,1
* 10000,-1,6
* 100000,-1,9
*
*/
class BenchJavaSort{
    public static void main(String[] args){
        int sizea=1000;
        int sizeb=10000;
        int sizec=100000;
        int[] arra=new int[sizea];
        int[] arrb=new int[sizeb];
        int[] arrc=new int[sizec];
        double max=1000.00; // maximum value of an item in array
        int i=0;
        long timea=0;
        long timeb=0;
        long timec=0;

        System.out.println("arrLength,steps,time");
        for(i=0;i<arra.length;i++){
            arra[i]=(int)(Math.random()*max);
        }
        for(i=0;i<arrb.length;i++){
            arrb[i]=(int)(Math.random()*max);
        }
        for(i=0;i<arrc.length;i++){
            arrc[i]=(int)(Math.random()*max);
        }
        timea=System.currentTimeMillis();
        Arrays.sort(arra);
        timea=System.currentTimeMillis()-timea;
        timeb=System.currentTimeMillis();
        Arrays.sort(arrb);
        timeb=System.currentTimeMillis()-timeb;
        timec=System.currentTimeMillis();
        Arrays.sort(arrc);
        timec=System.currentTimeMillis()-timec;
        System.out.println(sizea+",-1"+","+timea);
        System.out.println(sizeb+",-1"+","+timeb);
        System.out.println(sizec+",-1"+","+timec);
    }
}
