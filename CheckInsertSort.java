/*
*
* CheckInsertSort class
*
* check sort of an array of integers using insertion sort
*
* ----- compile -----
*
* javac CheckInsertSort.java
*
* ----- run -----
*
* java CheckInsertSort
*
*/
class CheckInsertSort{
    public static void main(String args[]){
        InsertSort sort=new InsertSort();
        Bench b=new Bench();
        int[] arr=new int[17];
        int i;
        double max=100.00;

        for(i=0;i<arr.length;i++){
            arr[i]=(int)(Math.random()*max);
            System.out.print(arr[i]+" ");
        }
        System.out.println("\n----- sorting array -----");
        sort.sort(arr,b);
        System.out.println("----- done sorting array -----");
        for(i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println("\n Number of steps "+b.count);
    }
}
