/*
*
* InsertSort class
*
* sort an array of integers using insertion sort
*
* quadratic complexity
*
* length of array n
*
* number of steps required to finish sort n^2
*
*/
class InsertSort{
    public void sort(int[] arr,Bench b){ // sort array of integers
        int i,j,tmp;
        for(i=1;i<arr.length;i++){ // go through each element of array
            tmp=arr[i];
            for(j=i-1;j>=0;j--){
                if(arr[j]>tmp){
                    arr[j+1]=arr[j];
                    b.count++;
                } else {
                    break;
                }
            }
            arr[j+1]=tmp; // put element in correct place
        }
    }
}
