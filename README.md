Assignment 1
------------

- BenchInsertSort.java (file, check comments to compile and run, benchmark insert sort)
- Bench.java (file, used to count number of steps of sort algorithm)
- BenchJavaSort.java (file, check comments to compile and run, benchmark default java sort)
- CheckInsertSort.java (file, example of insert sort, check comments to compile and run)
- InsertSort.java (file, implementation of insert sort for integers)

Test environment
----------------

- os lubuntu 16.04 lts kernel version 4.13.0
- javac version 11.0.5
